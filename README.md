# Fleet Operations

This repository is intended for applying [Fleet Infrastructure](https://gitlab.com/h2264/operations/fleet-infra) application runtime configuration.

## Getting Started

```bash
#!/bin/bash

# Validate argocd deployment(s)
kubectl -n $NAMESPACE get pods

# Get credentials
kubectl -n argocd get secret argocd-initial-admin-secret -o yaml | grep -o 'password: .*' | sed -e s"/password\: //g" | base64 -d

# Proxy the argocd-server
kubectl -n $NAMESPACE port-forward svc/argocd-server -n argocd 8080:443

# Login to argocd
argocd login localhost:8080

# Self manage the argocd app
argocd app create fleet-ops \
    --dest-namespace argocd \
    --dest-server https://kubernetes.default.svc \
    --repo https://gitlab.com/h2264/operations/fleet-ops.git \
    --path apps  
argocd app sync fleet-ops
```

## Applications in Scope

| Name | Installation Type | Status | Comment |
| --- | --- | --- | --- |
| dnsfix | helm | ![dnsfix](https://cd.cloudycloud.live/api/badge?name=dnsfix&revision=true&showAppName=true) | DNS registry manager. |
| k3s-upgrader-plans | kustomize | ![k3s-upgrader-plans](https://cd.cloudycloud.live/api/badge?name=k3s-upgrader-plans&revision=true&showAppName=true) | Certificate issuers used by ingress resources. |
| webhooks | kustomize | ![webhooks]( https://cd.cloudycloud.live/api/badge?name=webhooks&revision=true&showAppName=true) | Webhook service managed by argo-events |
| workflow-templates | kustomize | ![workflow-templates]( https://cd.cloudycloud.live/api/badge?name=workflow-templates&revision=true&showAppName=true) | Workflow templates managed by argo workflows |
|  |  |  |  |
